﻿using ShowStopper.Api.Contracts;
using Newtonsoft.Json;
using ShowStopper.Models;
using System.Linq;
namespace ShowStopper.Api.Implementation
{
    public class ShowReader : IShowReader
    {
        public ResponseModel ReadRequest(string jsonBody)
        {
            //create an instance of response object
            ResponseModel response = new ResponseModel();
            //Read full json request and deserialize into RequestModel
            var listOfShows = JsonConvert.DeserializeObject<RequestModel>(jsonBody);

            //check if listOfShows is not null
            if (listOfShows != null)
                //check if listOfShows.payload is not null
                if (listOfShows.payload != null)
                    //check if list of shows should be more than 0
                    if (listOfShows.payload.Count > 0)
                    {
                        //filter shows based on drm is true and episode count greater than 0
                        response.Shows = (from f in listOfShows.payload
                                          where f.drm = true && f.episodeCount > 0
                                          select new ShowResponse()
                                          {
                                              slug = f.slug,
                                              title = f.title,
                                              image = f.image.showImage
                                          }).ToList();
                    }
            return response;
        }

    }
}