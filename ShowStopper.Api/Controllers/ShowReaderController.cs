﻿using System.Web.Http;
using ShowStopper.Api.Contracts;
using ShowStopper.Api.Implementation;
using ShowStopper.Models;
using System.Net.Http;
using System.Net;
using System.Threading.Tasks;
using System;
using Newtonsoft.Json;

namespace ShowStopper.Api.Controllers
{
    public class ShowReaderController : ApiController
    {
        /// <summary>
        /// Reads and filter the json request 
        /// </summary>
        /// <returns></returns>
        /// <exception cref="System.Web.Http.HttpResponseException"></exception>
        [HttpPost()]
        public async Task<ResponseModel> Read()
        {
            try
            {
                //creating an instance of showreader implementation
                ///<interface cref = "IShowReader" ></interface>
                IShowReader showRdr = new ShowReader();

                //read json body asynchronously
                string jsonBody = await Request.Content.ReadAsStringAsync();
                // reading converting json body into object, filter out the json keys and return the response
                ///<class cref="ShowReader"></class>
                var response = showRdr.ReadRequest(jsonBody);
                return response;
            }
            catch (Exception)
            {
                //Create a bad request http response
                HttpResponseMessage errResponse = Request.CreateResponse(HttpStatusCode.BadRequest, new ErrorModel()
                {
                    ErrorMessage = "Could not decode request: JSON parsing failed"
                });

                throw new HttpResponseException(errResponse);
            };
        }

        /// <summary>
        /// Determines whether this Service is Alive.
        /// </summary>
        /// <returns></returns>
        [HttpGet()]
        public bool IsAlive()
        {
            return true;
        }
    }
}