﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json.Serialization;
using System.Net.Http.Formatting;

namespace ShowStopper
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            MediaTypeFormatterCollection mediaTypeFormatters = config.Formatters;
            mediaTypeFormatters.Remove(mediaTypeFormatters.XmlFormatter);

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "",
                routeTemplate: "{controller}/{action}/",
                defaults: new { controller = "ShowReader", action = "Read" }

            );
        }
    }
}
