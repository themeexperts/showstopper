﻿using ShowStopper.Models;
using System.Collections.Generic;

namespace ShowStopper.Api.Contracts
{
    public interface IShowReader
    {
        /// <summary>
        /// Reads the request.
        /// </summary>
        /// <param name="jsonBody">The json body.</param>
        ResponseModel ReadRequest(string jsonBody);
    }
}