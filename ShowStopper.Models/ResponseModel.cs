﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ShowStopper.Models
{
    public class ResponseModel
    {
        /// <summary>
        /// Gets or sets the shows.
        /// </summary>
        /// <value>
        /// The shows.
        /// </value>
        [JsonProperty("response", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public List<ShowResponse> Shows { get; set; }
    }

    public class ShowResponse
    {
        public string image { get; set; }
        public string slug { get; set; }
        public string title { get; set; }
    }
}
