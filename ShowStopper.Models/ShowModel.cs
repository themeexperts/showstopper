﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShowStopper.Models
{
    public class ShowModel
    {
        /// <summary>
        /// Gets or sets the country.
        /// </summary>
        /// <value>
        /// The country.
        /// </value>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string country { get; set; }
        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string description { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="Show"/> is DRM.
        /// </summary>
        /// <value>
        ///   <c>true</c> if DRM; otherwise, <c>false</c>.
        /// </value>
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public bool drm { get; set; }
        /// <summary>
        /// Gets or sets the episode count.
        /// </summary>
        /// <value>
        /// The episode count.
        /// </value>
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public int episodeCount { get; set; }
        /// <summary>
        /// Gets or sets the genre.
        /// </summary>
        /// <value>
        /// The genre.
        /// </value>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string genre { get; set; }
        /// <summary>
        /// Gets or sets the image.
        /// </summary>
        /// <value>
        /// The image.
        /// </value>
        public Image image { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string language { get; set; }
        /// <summary>
        /// Gets or sets the primary colour.
        /// </summary>
        /// <value>
        /// The primary colour.
        /// </value>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string primaryColour { get; set; }
        public string slug { get; set; }
        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        /// <value>
        /// The title.
        /// </value>
        public string title { get; set; }
        /// <summary>
        /// Gets or sets the tv channel.
        /// </summary>
        /// <value>
        /// The tv channel.
        /// </value>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string tvChannel { get; set; }
    }
    public class Image
    {
        /// <summary>
        /// Gets or sets the show image.
        /// </summary>
        /// <value>
        /// The show image.
        /// </value>
        public string showImage { get; set; }
    }
}
