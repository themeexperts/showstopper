﻿using Newtonsoft.Json;

namespace ShowStopper.Models
{
    public class ErrorModel
    {
        [JsonProperty("error")]
        public string ErrorMessage { get; set; }

    }
}
