﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShowStopper.Api.Implementation;
using ShowStopper.Api.Contracts;
using System.IO;
using ShowStopper.Models;

namespace ShowStopper.Tests
{
    [TestClass]
    public class JsonReaderTest
    {
        string jsonBasePath = string.Format("{0}\\{1}", AppDomain.CurrentDomain.BaseDirectory, "test_data");
        [TestMethod]
        public void JsonReader_CheckIfJsonDeserializingTheObject()
        {
            string jsonPath = string.Format("{0}\\{1}", jsonBasePath, "request.json");
            string jsonString = File.ReadAllText(jsonPath);
            IShowReader showReader = new ShowReader();
            var res = showReader.ReadRequest(jsonString);
            Assert.AreEqual(res.Shows.Count > 0, true);
        }

        [TestMethod]
        public void JsonReader_CheckIfJsonIsNotNullOrEmpty()
        {

            IShowReader showReader = new ShowReader();
            var res = showReader.ReadRequest("");
            Assert.AreEqual(res.Shows, null);
        }

        [TestMethod]
        public void JsonReader_CheckForJsonFormatBadRequest()
        {
            ErrorModel err = new ErrorModel();
            //bad format of the json string
            try
            {
                string jsonPath = string.Format("{0}\\{1}", jsonBasePath, "bad-request.json");
                string jsonString = File.ReadAllText(jsonPath);
                IShowReader showReader = new ShowReader();
                var res = showReader.ReadRequest(jsonString);
            }
            catch (Exception)
            {
                err.ErrorMessage = "Could not decode request: JSON parsing failed";
            }

            Assert.AreEqual(err.ErrorMessage, "Could not decode request: JSON parsing failed");
        }

        [TestMethod]
        public void JsonReader_CheckIfJsonDoesNotHavePayloadRootNode()
        {
            string jsonPath = string.Format("{0}\\{1}", jsonBasePath, "bad-request-without-payload-node.json");
            string jsonString = File.ReadAllText(jsonPath);
            IShowReader showReader = new ShowReader();
            var res = showReader.ReadRequest(jsonString);
            Assert.AreEqual(res.Shows, null);
        }

    }
}